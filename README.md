# [Python en la Nube: Automatizando la operación de AWS con Boto3](https://pycon.cl)

## PyCon Chile 2022 - 4 de Diciembre

[![PyCon Chile 2022](images/PyConChile2022Hero.jpeg)](https://pycon.cl)

## David Sol

### Wizeline Site Reliability Engineer

### Twitter: [@soldavidcloud](https://twitter.com/soldavidcloud)

### Repositorio: <https://gitlab.com/soldavid/pyconchile20221204>
